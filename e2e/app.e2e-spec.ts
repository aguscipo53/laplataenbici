import { LaPlataEnBiciPage } from './app.po';

describe('la-plata-en-bici App', () => {
  let page: LaPlataEnBiciPage;

  beforeEach(() => {
    page = new LaPlataEnBiciPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
